import unittest
import config
from hamcrest import *
from rest_framework import status
import email
import requests
import hashlib
import hmac
import base64
import xmlrunner


def make_digest(message, key):
    key = bytes(key, 'UTF-8')
    message = bytes(message, 'UTF-8')

    digester = hmac.new(key, message, hashlib.sha1)
    # signature1 = digester.hexdigest()
    signature1 = digester.digest()
    # print(signature1)

    # signature2 = base64.urlsafe_b64encode(bytes(signature1, 'UTF-8'))
    signature2 = base64.b64encode(signature1)
    # print(signature2)

    return str(signature2, 'UTF-8')


class TestQuintRecommendations(unittest.TestCase):

    def setUp(self):
        self.stage_url = config.STAGING_RRS_API_URL

    def test_article_recommendations_location_wise_without_merchant_id_and_source_details(self):

        querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167"}

        uri = "application/json,/?ip={0}&uid={1},{2}".format(querystring['ip'],
                                                                   querystring['uid'],
                                                                   email.utils.formatdate(usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_article_recommendations_location_wise_without_user_id_and_source_details(self):

        self.stage_url += config.QUINTYPE_MERCHANT_ID

        querystring = {"ip": "106.51.77.167"}

        uri = "application/json,/130915?ip={0},{1}".format(querystring['ip'],
                                                            email.utils.formatdate(usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_article_recommendations_location_wise_with_invalid_merchant_id(self):

        self.stage_url += "130976"

        querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167"}

        uri = "application/json,/130976?ip={0}&uid={1},{2}".format(querystring['ip'],
                                                                   querystring['uid'],
                                                                   email.utils.formatdate(usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_article_recommendations_location_wise_without_source_details(self):

        self.stage_url += config.QUINTYPE_MERCHANT_ID

        querystring = {"ip": "106.51.77.167", "uid": "5961a801-2d71-306b-8900-10e9444e1a78"}

        uri = "application/json,/130915?ip={0}&uid={1},{2}".format(querystring['ip'],
                                                                   querystring['uid'],
                                                                   email.utils.formatdate(usegmt=True))

        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, headers=headers, params=querystring)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response_dict = response.json()
        for dictionary in response_dict:
            assert_that(dictionary, has_key("currentData"))
            assert_that(dictionary['currentData'], has_key("Timestamp"))
            assert_that(dictionary['currentData'], has_key("ImageUrl"))
            assert_that(dictionary['currentData'], has_key("ImageMetaData"))
            assert_that(dictionary['currentData'], has_key("AuthorName"))
            try:
                assert_that(dictionary, has_key("recommendations"))
                for recommendation in dictionary['recommendations']:
                    assert_that(recommendation, has_key("Cid"))
                    try:
                        assert_that(recommendation, has_key("SimsScore"))
                    except Exception as e:
                        pass
                    assert_that(recommendation, has_key("Timestamp"))
                    assert_that(recommendation, has_key("ImageUrl"))
                    assert_that(recommendation, has_key("ImageMetaData"))
                    assert_that(recommendation, has_key("AuthorName"))
            except Exception as e:
                pass

    def test_article_recommendations_location_wise_with_source_details_without_merchant_id(self):

        querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "source": "true"}

        uri = "application/json,/?ip={0}&uid={1}&source={2},{3}".format(querystring['ip'],
                                                                        querystring['source'],
                                                                        querystring['uid'],
                                                                        email.utils.formatdate(usegmt=True))

        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_article_recommendations_location_wise_with_source_details_without_user_id(self):

        self.stage_url += config.QUINTYPE_MERCHANT_ID

        querystring = {"ip": "106.51.77.167", "source": "true"}

        uri = "application/json,/130915?ip={0}&source={1},{2}".format(querystring['ip'],
                                                                      querystring['source'],
                                                                      email.utils.formatdate(usegmt=True))

        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_article_recommendations_location_wise_with_source_details_and_invalid_merchant_id(self):

        self.stage_url += "130956"

        querystring = {"ip": "106.51.77.167", "source": "true", "uid": "5961a801-2d71-306b-8900-10e9444e1a78"}

        uri = "application/json,/130956?uid={0}&ip={1}&source={2},{3}".format(querystring['uid'], querystring['ip'],
                                                                              querystring['source'],
                                                                              email.utils.formatdate(usegmt=True))

        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_article_recommendations_location_wise_with_source_details(self):

        self.stage_url += "130915"

        querystring = {"ip": "106.51.77.167", "uid": "5961a801-2d71-306b-8900-10e9444e1a78", "source": "true"}

        uri = "application/json,/130915?ip={0}&uid={1}&source={2},{3}".format(querystring['ip'], querystring['uid'],
                                                                              querystring['source'],
                                                                              email.utils.formatdate(usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_dict = response.json()

        for dictionary in response_dict:
            assert_that(dictionary, has_key("currentData"))
            assert_that(dictionary['currentData'], has_key("Timestamp"))
            assert_that(dictionary['currentData'], has_key("ImageUrl"))
            assert_that(dictionary['currentData'], has_key("ImageMetaData"))
            assert_that(dictionary['currentData'], has_key("AuthorName"))
            try:
                assert_that(dictionary, has_key("recommendations"))
                for recommendation in dictionary['recommendations']:
                    assert_that(recommendation, has_key("Cid"))
                    try:
                        assert_that(recommendation, has_key("SimsScore"))
                    except Exception as e:
                        pass
                    assert_that(recommendation, has_key("Description"))
                    assert_that(recommendation, has_key("Category"))
                    assert_that(recommendation, has_key("Tags"))
                    assert_that(recommendation, has_key("Slug"))
                    assert_that(recommendation, has_key("Model"))
                    assert_that(recommendation, has_key("Reason"))
                    assert_that(recommendation, has_key("Analysis"))
                    assert_that(recommendation, has_key("Timestamp"))
                    assert_that(recommendation, has_key("ImageUrl"))
                    assert_that(recommendation, has_key("ImageMetaData"))
                    assert_that(recommendation, has_key("AuthorName"))
            except Exception as e:
                pass

    # def test_video_recommendations_location_wise_without_source_details_without_merchant_id(self):
    #
    #     querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    #
    # def test_video_recommendations_location_wise_without_source_details_without_user_id(self):
    #
    #     self.stage_url += config.QUINTYPE_MERCHANT_ID
    #
    #     querystring = {"ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=config.headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    #
    # def test_video_recommendations_location_wise_without_source_details_and_invalid_merchant_id(self):
    #
    #     self.stage_url += "130967"
    #
    #     querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=config.headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # def test_video_recommendation_location_wise_without_source_details(self):
    #
    #     self.stage_url += config.QUINTYPE_MERCHANT_ID
    #
    #     querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=config.headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #
    #     for dictionary in response:
    #         assert_that(dictionary, has_key("recommendations"))
    #         assert_that(dictionary, has_key("currentData"))
    #         assert_that(dictionary['currentData'], has_key("Timestamp"))
    #         assert_that(dictionary['currentData'], has_key("ImageUrl"))
    #         assert_that(dictionary['currentData'], has_key("ImageMetaData"))
    #         assert_that(dictionary['currentData'], has_key("AuthorName"))
    #         for recommendation in dictionary['recommendations']:
    #             assert_that(recommendation, has_key("Cid"))
    #             assert_that(recommendation, has_key("Timestamp"))
    #             assert_that(recommendation, has_key("ImageUrl"))
    #             assert_that(recommendation, has_key("ImageMetaData"))
    #             assert_that(recommendation, has_key("AuthorName"))
    #
    # def test_video_recommendations_location_wise_with_source_details_without_merchant_id(self):
    #
    #     querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=config.headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
    #
    # def test_video_recommendations_location_wise_with_source_details_without_user_id(self):
    #
    #     self.stage_url += config.QUINTYPE_MERCHANT_ID
    #
    #     querystring = {"ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=config.headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    #
    # def test_video_recommendations_location_wise_with_source_details_and_invalid_merchant_id(self):
    #
    #     self.stage_url += "130967"
    #
    #     querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=config.headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    #
    # def test_video_recommendation_location_wise_with_source_details(self):
    #
    #     self.stage_url += config.QUINTYPE_MERCHANT_ID
    #
    #     querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "vsims"}
    #
    #     response = requests.request("GET", self.stage_url, params=querystring, headers=config.headers)
    #
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #
    #     for dictionary in response:
    #         assert_that(dictionary, has_key("recommendations"))
    #         assert_that(dictionary, has_key("currentData"))
    #         assert_that(dictionary['currentData'], has_key("Timestamp"))
    #         assert_that(dictionary['currentData'], has_key("ImageUrl"))
    #         assert_that(dictionary['currentData'], has_key("ImageMetaData"))
    #         assert_that(dictionary['currentData'], has_key("AuthorName"))
    #         for recommendation in dictionary['recommendations']:
    #             assert_that(recommendation, has_key("Cid"))
    #             assert_that(recommendation, has_key("SimsScore"))
    #             assert_that(recommendation, has_key("Description"))
    #             assert_that(recommendation, has_key("Category"))
    #             assert_that(recommendation, has_key("Tags"))
    #             assert_that(recommendation, has_key("Slug"))
    #             assert_that(recommendation, has_key("Model"))
    #             assert_that(recommendation, has_key("Reason"))
    #             assert_that(recommendation, has_key("Analysis"))
    #             assert_that(recommendation, has_key("Timestamp"))
    #             assert_that(recommendation, has_key("Thumbnail"))
    #             assert_that(recommendation, has_key("ImageUrl"))
    #             assert_that(recommendation, has_key("ImageMetaData"))
    #             assert_that(recommendation, has_key("AuthorName"))

    def test_content_to_content_based_article_recommendations_location_wise_with_source_details_without_merchant_id(self):

        querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "csims",
                       "cid": "4e5d9566-784d-4123-ad80-403b3f57f508", "source": "true"}
        uri = "application/json,/?uid={0}&ip={1}&st={2}&cid={3}&source=true,{4}".format(querystring['uid'],
                                                                                        querystring['ip'],
                                                                                        querystring['st'],
                                                                                        querystring['cid'],
                                                                                        email.utils.formatdate(
                                                                                            usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_content_to_content_based_article_recommendations_location_wise_with_source_details_invalid_merchant_id(self):

        self.stage_url += "130987"

        querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "csims",
                       "cid": "4e5d9566-784d-4123-ad80-403b3f57f508", "source": "true"}
        uri = "application/json,/130915?uid={0}&ip={1}&st={2}&cid={3}&source=true,{4}".format(querystring['uid'],
                                                                                              querystring['ip'],
                                                                                              querystring['st'],
                                                                                              querystring['cid'],
                                                                                              email.utils.formatdate(
                                                                                                  usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_content_to_content_based_article_recommendations_location_wise_with_source_details_without_user_id(self):

        self.stage_url += config.QUINTYPE_MERCHANT_ID

        querystring = {"ip": "106.51.77.167", "st": "csims",
                       "cid": "4e5d9566-784d-4123-ad80-403b3f57f508", "source": "true"}
        uri = "application/json,/130915?ip={0}&st={1}&cid={2}&source=true,{3}".format(
            querystring['ip'],
            querystring['st'],
            querystring['cid'],
            email.utils.formatdate(
                usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, headers=headers, params=querystring)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_content_to_content_based_article_recommendations_location_wise_with_source_details_with_invalid_stratergy(
            self):

        self.stage_url += config.QUINTYPE_MERCHANT_ID

        querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "csims",
                       "cid": "4e5d9566-784d-4123-ad80-403b3f57f508", "source": "true"}
        uri = "application/json,/130915?uid={0}&ip={1}&st={2}&cid={3}&source=true,{4}".format(querystring['uid'],
                                                                                              querystring['ip'],
                                                                                              querystring['st'],
                                                                                              querystring['cid'],
                                                                                              email.utils.formatdate(
                                                                                                  usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)

        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def test_content_to_content_based_article_recommendation_location_wise_with_source_details(self):

        self.stage_url += config.QUINTYPE_MERCHANT_ID

        querystring = {"uid": "5961a801-2d71-306b-8900-10e9444e1a78", "ip": "106.51.77.167", "st": "isims",
                       "cid": "4e5d9566-784d-4123-ad80-403b3f57f508", "source": "true"}
        uri = "application/json,/130915?uid={0}&ip={1}&st={2}&cid={3}&source=true,{4}".format(querystring['uid'],
                                                                                              querystring['ip'],
                                                                                              querystring['st'],
                                                                                              querystring['cid'],
                                                                                              email.utils.formatdate(
                                                                                                  usegmt=True))
        token = make_digest(uri, "keygen")

        headers = {
            'Authorization': "Authorization me:{0}".format(token),
            'DateUsed': "{0}".format(email.utils.formatdate(usegmt=True)),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cache-Control': "no-cache",
            'Host': "avenger.wru.ai",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", self.stage_url, params=querystring, headers=headers)
        response_dict = response.json()

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        for dictionary in response_dict:
            assert_that(dictionary, has_key("currentData"))
            assert_that(dictionary['currentData'], has_key("Cid"))
            assert_that(dictionary['currentData'], has_key("Description"))
            assert_that(dictionary['currentData'], has_key("Category"))
            assert_that(dictionary['currentData'], has_key("Tags"))
            assert_that(dictionary['currentData'], has_key("Slug"))
            assert_that(dictionary['currentData'], has_key("Timestamp"))
            assert_that(dictionary['currentData'], has_key("ImageUrl"))
            assert_that(dictionary['currentData'], has_key("ImageMetaData"))
            assert_that(dictionary['currentData'], has_key("AuthorName"))
            try:
                assert_that(dictionary, has_key("recommendations"))
                for recommendation in dictionary['recommendations']:
                    assert_that(recommendation, has_key("Cid"))
                    assert_that(recommendation, has_key("SimsScore"))
                    assert_that(recommendation, has_key("Description"))
                    assert_that(recommendation, has_key("Category"))
                    assert_that(recommendation, has_key("Tags"))
                    assert_that(recommendation, has_key("Slug"))
                    assert_that(recommendation, has_key("Timestamp"))
                    assert_that(recommendation, has_key("ImageUrl"))
                    assert_that(recommendation, has_key("ImageMetaData"))
                    assert_that(recommendation, has_key("AuthorName"))
            except Exception as e:
                pass

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output="Test Reports"))
